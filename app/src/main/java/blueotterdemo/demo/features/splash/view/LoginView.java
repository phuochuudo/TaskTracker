package blueotterdemo.demo.features.splash.view;

import blueotterdemo.demo.features.login.model.LoginResponse;

/**
 * Created by PhuocDH on 3/14/2018.
 */

public interface LoginView {

    void showLoading();

    void hideLoading();

    void showError(String error);

    void login(LoginResponse loginResponse);
}
