package blueotterdemo.demo.features.splash.presenter;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestBatch;
import com.facebook.GraphResponse;
import com.google.gson.Gson;
import blueotterdemo.demo.features.login.model.LoginResponse;
import blueotterdemo.demo.features.splash.view.LoginView;

/**
 * Created by PhuocDH on 3/14/2018.
 */

public class LoginPresenterImpl implements LoginPresenter {
    private LoginView mView;
    public LoginPresenterImpl( LoginView view) {
        this.mView = view;
    }

    @Override
    public void doLogin(AccessToken accessToken) {
        mView.showLoading();
        GraphRequest request = GraphRequest.newGraphPathRequest(accessToken, "/me/", new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse response) {
                mView.hideLoading();
                if (response.getJSONObject() == null) {
                    mView.login(null);
                } else {
                    mView.login(new Gson().fromJson(response.getJSONObject().toString(), LoginResponse.class));
                }
            }
        });
        GraphRequest.executeBatchAsync(new GraphRequestBatch(request));
    }
}
