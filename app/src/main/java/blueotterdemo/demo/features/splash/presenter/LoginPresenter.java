package blueotterdemo.demo.features.splash.presenter;

import com.facebook.AccessToken;

/**
 * Created by PhuocDH on 3/14/2018.
 */

public interface LoginPresenter  {
    void doLogin(AccessToken accessToken);
}
