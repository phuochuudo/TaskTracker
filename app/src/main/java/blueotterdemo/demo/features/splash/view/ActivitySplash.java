package blueotterdemo.demo.features.splash.view;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;

import com.facebook.AccessToken;

import blueotterdemo.demo.R;
import blueotterdemo.demo.base.BaseActivity;
import blueotterdemo.demo.features.login.model.LoginResponse;
import blueotterdemo.demo.features.login.view.ActivityLoginFace;
import blueotterdemo.demo.features.main.view.ActivityMain;
import blueotterdemo.demo.features.splash.presenter.LoginPresenter;
import blueotterdemo.demo.features.splash.presenter.LoginPresenterImpl;
import blueotterdemo.demo.utils.ToastUtil;
import butterknife.BindView;
import butterknife.ButterKnife;

import static blueotterdemo.demo.utils.Constants.USER_ID;

/**
 * Created by PhuocDH on 3/8/2018.
 */

public class ActivitySplash extends BaseActivity implements LoginView {
    @BindView(R.id.llSplash)
    LinearLayout llSplash;
    private LoginPresenter mPresenter;

    @Override
    protected void initData(Bundle savedInstanceState) {
        // GET bundle data if existing
    }

    @Override
    protected void initRootView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        mPresenter = new LoginPresenterImpl(this);
    }

    @Override
    protected void initUI(Bundle savedInstanceState) {
        llSplash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.doLogin(AccessToken.getCurrentAccessToken());
            }
        });
    }

    @Override
    protected void loadData(Bundle savedInstanceState) {
    }

    @Override
    public void showError(String error) {
        ToastUtil.getInstance().show(this, error);
    }

    @Override
    public void login(LoginResponse loginResponse) {
        Intent intent;
        if (loginResponse == null) {
            intent = new Intent(ActivitySplash.this, ActivityLoginFace.class);
        } else {
            if (TextUtils.isEmpty(loginResponse.getId())) {
                intent = new Intent(ActivitySplash.this, ActivityLoginFace.class);
            } else {
                intent = new Intent(ActivitySplash.this, ActivityMain.class);
                Bundle bundle = new Bundle();
                bundle.putString(USER_ID, loginResponse.getId());
                intent.putExtras(bundle);
            }
        }
        startActivity(intent);
        finish();
    }
}
