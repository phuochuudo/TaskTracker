package blueotterdemo.demo.features.login.presenter;

import android.app.Activity;
import android.content.Context;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.util.Arrays;

import blueotterdemo.demo.features.login.model.LoginResponse;
import blueotterdemo.demo.features.login.view.LoginFaceView;

/**
 * Created by PhuocDH on 3/14/2018.
 */

public class LoginFacePresenterImpl implements LoginFacePresenter {
    private LoginFaceView mView;
    private Context mContext;

    public LoginFacePresenterImpl(Context context, LoginFaceView view) {
        this.mContext = context;
        this.mView = view;
    }

    @Override
    public void doLoginFace(CallbackManager callbackManager) {
        LoginManager.getInstance().logInWithReadPermissions((Activity) mContext, Arrays.asList("email", "public_profile"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                if (loginResult != null) {
                    LoginResponse loginResponse = new LoginResponse();
                    loginResponse.setId(loginResult.getAccessToken().getUserId());
                    mView.login(loginResponse);
                } else {
                    mView.showError("Login Facebook fail !");
                }
            }

            @Override
            public void onCancel() {
                LoginManager.getInstance().logOut();
                mView.showError("Login Facebook fail !");
            }

            @Override
            public void onError(FacebookException error) {
                LoginManager.getInstance().logOut();
                mView.showError("Login Facebook fail !");
            }
        });
    }
}
