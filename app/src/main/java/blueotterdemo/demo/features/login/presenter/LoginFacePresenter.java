package blueotterdemo.demo.features.login.presenter;

import com.facebook.CallbackManager;

/**
 * Created by PhuocDH on 3/14/2018.
 */

public interface LoginFacePresenter {
    void doLoginFace(CallbackManager callbackManager);
}
