package blueotterdemo.demo.features.login.view;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;

import com.facebook.CallbackManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import blueotterdemo.demo.R;
import blueotterdemo.demo.base.BaseActivity;
import blueotterdemo.demo.features.login.model.LoginResponse;
import blueotterdemo.demo.features.login.presenter.LoginFacePresenter;
import blueotterdemo.demo.features.login.presenter.LoginFacePresenterImpl;
import blueotterdemo.demo.features.main.view.ActivityMain;
import blueotterdemo.demo.utils.ToastUtil;
import butterknife.BindView;
import butterknife.ButterKnife;

import static blueotterdemo.demo.utils.Constants.USER_ID;

/**
 * Created by PhuocDH on 3/14/2018.
 */

public class ActivityLoginFace extends BaseActivity implements LoginFaceView {
    private static final String TAG = ActivityLoginFace.class.getSimpleName();
    private LoginFacePresenter mPresenter;

    @BindView(R.id.btnLoginByFB)
    LinearLayout btnLoginByFB;
    private CallbackManager callbackManager;

    @Override
    protected void initData(Bundle savedInstanceState) {

    }

    @Override
    protected void initRootView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        mPresenter = new LoginFacePresenterImpl(this, this);
        callbackManager = CallbackManager.Factory.create();
    }

    @Override
    protected void initUI(Bundle savedInstanceState) {
        btnLoginByFB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.doLoginFace(callbackManager);
            }
        });
    }

    @Override
    protected void loadData(Bundle savedInstanceState) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result back to the Facebook SDK
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void showError(String error) {
        ToastUtil.getInstance().show(this, error);
    }

    @Override
    public void login(LoginResponse loginResponse) {
        if (!TextUtils.isEmpty(loginResponse.getId())) {
            Intent intent = new Intent(ActivityLoginFace.this, ActivityMain.class);
            Bundle bundle = new Bundle();
            bundle.putString(USER_ID, loginResponse.getId());
            intent.putExtras(bundle);
            startActivity(intent);
            finish();
        }
    }
}
