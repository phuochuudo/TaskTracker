package blueotterdemo.demo.features.login.view;

import blueotterdemo.demo.features.login.model.LoginResponse;

/**
 * Created by PhuocDH on 3/14/2018.
 */

public interface LoginFaceView {

    void showLoading();

    void hideLoading();

    void showError(String error);

    void login(LoginResponse loginResponse);
}
