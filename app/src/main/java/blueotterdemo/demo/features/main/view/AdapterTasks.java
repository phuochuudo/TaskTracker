package blueotterdemo.demo.features.main.view;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import blueotterdemo.demo.R;
import blueotterdemo.demo.base.AdapterBase;
import blueotterdemo.demo.base.ViewHolderBase;
import blueotterdemo.demo.features.main.model.TaskDetail;

import static blueotterdemo.demo.utils.Constants.TASK_SAVED;

/**
 * Created by PhuocDH on 3/13/2018.
 */

public class AdapterTasks extends AdapterBase<ViewHolderBase> {
    private static final int TYPE_TASK = 0;
    private static final int TYPE_TASK_DETAIL = 1;
    private List<TaskDetail> mListTask;
    private List<TaskDetail> mListTaskDetail;
    private OnClickItem mListener;
    private int mType = 0;

    public AdapterTasks(Context context, OnClickItem onClickItem) {
        super(context);
        mListTask = new ArrayList<>();
        mListener = onClickItem;
    }

    @Override
    public ViewHolderBase onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_TASK:
                return new AdapterTasks.TaskViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_task, parent, false));
            case TYPE_TASK_DETAIL:
                return new AdapterTasks.TaskDetailViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_task_detail, parent, false));
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(ViewHolderBase holder, int position) {
        switch (holder.getItemViewType()) {
            case TYPE_TASK:
                holder.bindData(mListTask.get(position));
                break;
            case TYPE_TASK_DETAIL:
                holder.bindData(mListTaskDetail.get(position));
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mType;
    }

    public void setListTaskDetail(List<TaskDetail> listTaskDetail) {
        this.mListTask.addAll(listTaskDetail);
        notifyDataSetChanged();
    }

    public void setListTaskDetail(TaskDetail taskDetail) {
        this.mListTask.add(taskDetail);
        notifyDataSetChanged();
    }

    public void setType(int type) {
        this.mType = type;
        if (type == TYPE_TASK_DETAIL) {
            mListTaskDetail = new ArrayList<>();
            for (TaskDetail detail : mListTask) {
                if (detail.isFlagSave()) {
                    mListTaskDetail.add(detail);
                }
            }
        }
        notifyDataSetChanged();
    }


    public void updateListTaskDetail(TaskDetail taskDetail) {
        for (TaskDetail detail : mListTask) {
            if (detail.getTaskId().equals(taskDetail.getTaskId())) {
                detail = taskDetail;
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (mType == TYPE_TASK) {
            return mListTask == null ? 0 : mListTask.size();
        } else {
            return mListTaskDetail == null ? 0 : mListTaskDetail.size();
        }
    }

    private class TaskViewHolder extends ViewHolderBase<TaskDetail> {
        Button btnTaskName;
        EditText edtTaskDescription;

        public TaskViewHolder(View itemView) {
            super(itemView);
            btnTaskName = itemView.findViewById(R.id.btnTaskName);
            edtTaskDescription = itemView.findViewById(R.id.edtTaskDescription);
        }

        @Override
        public void bindData(final TaskDetail taskDetail) {
            super.bindData(taskDetail);
            btnTaskName.setBackground(getResources().getDrawable(R.drawable.bg_circle_button_blue));
            btnTaskName.setText(taskDetail.getTaskName());
            edtTaskDescription.setHint(taskDetail.getTaskDescription());
            edtTaskDescription.setText("");
            btnTaskName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    btnTaskName.setBackground(getResources().getDrawable(R.drawable.bg_circle_button_green));
                    btnTaskName.setText(TASK_SAVED);
                    if (TextUtils.isEmpty(edtTaskDescription.getText().toString())) {
                        taskDetail.setTaskDescriptionSave(edtTaskDescription.getHint().toString());
                    } else {
                        taskDetail.setTaskDescriptionSave(edtTaskDescription.getText().toString());
                    }

                    mListener.onItemClick(taskDetail);
                }
            });
        }
    }

    private class TaskDetailViewHolder extends ViewHolderBase<TaskDetail> {
        TextView txtTaskName;
        TextView txtTaskDescriptionSave;
        TextView txtTaskTime;

        public TaskDetailViewHolder(View itemView) {
            super(itemView);
            txtTaskName = itemView.findViewById(R.id.txtTaskName);
            txtTaskDescriptionSave = itemView.findViewById(R.id.txtTaskDescriptionSave);
            txtTaskTime = itemView.findViewById(R.id.txtTaskTime);
        }

        @Override
        public void bindData(final TaskDetail taskDetail) {
            super.bindData(taskDetail);
            txtTaskName.setText(taskDetail.getTaskName());
            txtTaskDescriptionSave.setText(taskDetail.getTaskDescriptionSave());
            txtTaskTime.setText(taskDetail.getTime());
        }
    }

    public interface OnClickItem extends OnBaseItemClickListener<TaskDetail> {
    }
}
