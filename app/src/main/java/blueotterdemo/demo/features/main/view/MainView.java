package blueotterdemo.demo.features.main.view;

import java.util.List;

import blueotterdemo.demo.features.main.model.TaskDetail;

/**
 * Created by PhuocDH on 3/14/2018.
 */

public interface MainView {

    void showLoading();

    void hideLoading();

    void showError(String error);

    void getFireBaseData(List<TaskDetail> taskDetailList);

    void addFireBaseData(TaskDetail taskDetail);

    void updateFireBaseData(TaskDetail taskDetail);
}
