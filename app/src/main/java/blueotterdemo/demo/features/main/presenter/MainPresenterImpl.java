package blueotterdemo.demo.features.main.presenter;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import blueotterdemo.demo.features.main.model.TaskDetail;
import blueotterdemo.demo.features.main.view.MainView;

/**
 * Created by PhuocDH on 3/14/2018.
 */

public class MainPresenterImpl implements MainPresenter {

    private MainView mView;

    public MainPresenterImpl(MainView view) {
        this.mView = view;
    }

    @Override
    public void getFireBaseData(DatabaseReference databaseReference) {
        mView.showLoading();
        final GenericTypeIndicator<TaskDetail> typeIndicator = new GenericTypeIndicator<TaskDetail>() {
        };
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mView.hideLoading();
                List<TaskDetail> taskDetailList = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    TaskDetail taskDetail = snapshot.getValue(typeIndicator);
                    taskDetailList.add(taskDetail);
                }
                mView.getFireBaseData(taskDetailList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                mView.hideLoading();
                mView.showError(databaseError.getMessage());
            }
        });
    }

    @Override
    public void addFireBaseData(DatabaseReference databaseReference, final TaskDetail taskDetail) {
        mView.showLoading();
        String key = databaseReference.push().getKey();
        taskDetail.setTaskId(key);
        databaseReference.child(key).setValue(taskDetail).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                mView.hideLoading();
                mView.addFireBaseData(taskDetail);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                mView.hideLoading();
                mView.showError(e.getMessage());
            }
        });
    }

    @Override
    public void updateFireBaseData(DatabaseReference databaseReference, final TaskDetail taskDetail) {
        taskDetail.setFlagSave(true);
        taskDetail.setTime(new SimpleDateFormat().format(Calendar.getInstance().getTime()));
        databaseReference.child(taskDetail.getTaskId()).setValue(taskDetail).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                mView.updateFireBaseData(taskDetail);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                mView.showError(e.getMessage());
            }
        });
    }
}
