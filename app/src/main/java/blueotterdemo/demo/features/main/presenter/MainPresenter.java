package blueotterdemo.demo.features.main.presenter;

import com.google.firebase.database.DatabaseReference;

import blueotterdemo.demo.features.main.model.TaskDetail;

/**
 * Created by PhuocDH on 3/14/2018.
 */

public interface MainPresenter {
    void getFireBaseData(DatabaseReference databaseReference);
    void addFireBaseData(DatabaseReference databaseReference, TaskDetail taskDetail);
    void updateFireBaseData(DatabaseReference databaseReference, TaskDetail taskDetail);
}
