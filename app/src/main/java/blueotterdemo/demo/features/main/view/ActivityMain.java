package blueotterdemo.demo.features.main.view;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Arrays;
import java.util.List;

import blueotterdemo.demo.R;
import blueotterdemo.demo.base.BaseActivity;
import blueotterdemo.demo.features.main.model.TaskDetail;
import blueotterdemo.demo.features.main.presenter.MainPresenter;
import blueotterdemo.demo.features.main.presenter.MainPresenterImpl;
import blueotterdemo.demo.utils.ToastUtil;
import butterknife.BindView;
import butterknife.ButterKnife;

import static blueotterdemo.demo.utils.Common.hideSoftKeyboard;
import static blueotterdemo.demo.utils.Constants.USER_ID;

/**
 * Created by PhuocDH on 3/13/2018.
 */

public class ActivityMain extends BaseActivity implements MainView, AdapterTasks.OnClickItem {

    @BindView(R.id.rcvRecyclerViewTask)
    RecyclerView rcvRecyclerViewTask;
    @BindView(R.id.btnAddNewTask)
    Button btnAddNewTask;
    @BindView(R.id.txtTaskNoLog)
    TextView txtTaskNoLog;
    @BindView(R.id.txtTaskLog)
    TextView txtTaskLog;
    @BindView(R.id.txtTitleTask)
    TextView txtTitleTask;
    @BindView(R.id.imvClose)
    ImageView imvClose;

    private Context mContext;
    private AdapterTasks mAdapterTasks;
    private String mUserId;
    private DatabaseReference mDatabaseReference;
    private MainPresenter mPresenter;
    private Dialog mDialog;
    private boolean isFirst;
    private int mType = 0;

    @Override
    protected void initData(Bundle savedInstanceState) {
        mUserId = getIntent().getStringExtra(USER_ID);
    }

    @Override
    protected void initRootView(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);
        mContext = getContext();
        ButterKnife.bind(this);
        mDatabaseReference = FirebaseDatabase.getInstance().getReference().child(mUserId);
        mPresenter = new MainPresenterImpl(this);
    }

    @Override
    protected void initUI(Bundle savedInstanceState) {
        rcvRecyclerViewTask.setLayoutManager(new LinearLayoutManager(mContext));
        mAdapterTasks = new AdapterTasks(mContext, this);
        rcvRecyclerViewTask.setAdapter(mAdapterTasks);
        mPresenter.getFireBaseData(mDatabaseReference);
        btnAddNewTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mType == 1) {
                    hideView();
                }
                showDialogAddTask();
            }
        });

        txtTaskLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSoftKeyboard(mContext);
                mType = 1;
                mAdapterTasks.setType(mType);
                txtTitleTask.setVisibility(View.GONE);
                txtTaskLog.setVisibility(View.GONE);
                imvClose.setVisibility(View.VISIBLE);
            }
        });

        imvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideView();
            }
        });
    }

    @Override
    protected void loadData(Bundle savedInstanceState) {
    }

    @Override
    public void showError(String error) {
        ToastUtil.getInstance().show(this, error);
    }

    @Override
    public void getFireBaseData(List<TaskDetail> taskDetailList) {
        if (!isFirst) {
            if (taskDetailList == null || taskDetailList.isEmpty()) {
                txtTaskNoLog.setVisibility(View.VISIBLE);
                txtTitleTask.setVisibility(View.GONE);
                txtTaskLog.setVisibility(View.GONE);
            } else {
                txtTaskNoLog.setVisibility(View.GONE);
                txtTitleTask.setVisibility(View.VISIBLE);
                txtTaskLog.setVisibility(View.VISIBLE);
                mAdapterTasks.setListTaskDetail(taskDetailList);
            }
        }
    }

    @Override
    public void addFireBaseData(TaskDetail taskDetailList) {
        txtTaskNoLog.setVisibility(View.GONE);
        txtTitleTask.setVisibility(View.VISIBLE);
        txtTaskLog.setVisibility(View.VISIBLE);
        mDialog.dismiss();
        mAdapterTasks.setListTaskDetail(taskDetailList);
        isFirst = false;
    }

    @Override
    public void updateFireBaseData(TaskDetail taskDetail) {
        mAdapterTasks.updateListTaskDetail(taskDetail);
        isFirst = false;
    }

    private void showDialogAddTask() {
        mDialog = new Dialog(mContext, android.R.style.Theme_Black_NoTitleBar);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        mDialog.setContentView(R.layout.add_task);
        final EditText edtTaskName = mDialog.findViewById(R.id.edtTaskName);
        final EditText edtTaskDescription = mDialog.findViewById(R.id.edtTaskDescription);

        mDialog.findViewById(R.id.imvClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        mDialog.findViewById(R.id.btnAddTask).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TaskDetail taskDetail = new TaskDetail(edtTaskName.getText().toString(),
                        edtTaskDescription.getText().toString());
                if (!validateTask(taskDetail)) {
                    isFirst = true;
                    mPresenter.addFireBaseData(mDatabaseReference, taskDetail);
                }
            }
        });

        mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                hideSoftKeyboard(mContext);
            }
        });
        mDialog.show();
    }

    private boolean validateTask(TaskDetail taskDetail) {
        if (TextUtils.isEmpty(taskDetail.getTaskName())) {
            ToastUtil.getInstance().show(this, "Task name is empty !");
            return true;
        }

        if (TextUtils.isEmpty(taskDetail.getTaskDescription())) {
            ToastUtil.getInstance().show(this, "Task description is empty !");
            return true;
        }
        return false;
    }

    @Override
    public void onItemClick(TaskDetail taskDetail) {
        isFirst = true;
        mPresenter.updateFireBaseData(mDatabaseReference, taskDetail);
    }

    @Override
    public void onBackPressed() {
        if (mType == 1) {
            hideView();
        } else {
            super.onBackPressed();
        }
    }

    private void hideView() {
        hideSoftKeyboard(mContext);
        mType = 0;
        mAdapterTasks.setType(mType);
        txtTitleTask.setVisibility(View.VISIBLE);
        txtTaskLog.setVisibility(View.VISIBLE);
        imvClose.setVisibility(View.GONE);
    }
}
