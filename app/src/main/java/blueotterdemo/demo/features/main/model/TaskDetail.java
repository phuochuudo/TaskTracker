package blueotterdemo.demo.features.main.model;

/**
 * Created by PhuocDH on 3/13/2018.
 */

public class TaskDetail {

    private String taskId;
    private String taskName;
    private String taskDescription;
    private String taskDescriptionSave;
    private String time;
    private boolean flagSave;

    public TaskDetail() {
    }

    public TaskDetail(String taskName, String taskDescription) {
        this.taskName = taskName;
        this.taskDescription = taskDescription;
    }

    public String getTaskDescriptionSave() {
        return taskDescriptionSave;
    }

    public void setTaskDescriptionSave(String taskDescriptionSave) {
        this.taskDescriptionSave = taskDescriptionSave;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isFlagSave() {
        return flagSave;
    }

    public void setFlagSave(boolean flagSave) {
        this.flagSave = flagSave;
    }
}
