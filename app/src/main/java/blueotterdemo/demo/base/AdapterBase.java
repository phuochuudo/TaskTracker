package blueotterdemo.demo.base;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;

public abstract class AdapterBase<VH extends ViewHolderBase> extends RecyclerView.Adapter<VH> {
    private final Context mContext;

    protected AdapterBase(Context context) {
        mContext = context;
    }

    protected Resources getResources() {
        return mContext.getResources();
    }

    public interface OnBaseItemClickListener<Item> {
        void onItemClick(Item item);
    }
}
