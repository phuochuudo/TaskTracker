package blueotterdemo.demo.base;

import android.content.Intent;
import android.support.v4.app.Fragment;

/**
 * ***************************************
 * ❖ Created by PhuocDH on 03/08/18
 * ***************************************
 **/

public abstract class BaseFragment extends Fragment {

    public BaseFragment() {

    }

    public void startActivity(Class<?> cls) {
        Intent intent = new Intent(getContext(), cls);
        startActivity(intent);
    }
}