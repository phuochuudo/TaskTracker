package blueotterdemo.demo.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;
import blueotterdemo.demo.base.view.DLoading;

/**
 * ***************************************
 * ❖ Created by PhuocDH on 03/08/18
 * ***************************************
 **/

public abstract class BaseActivity extends AppCompatActivity {
    private DLoading loading;

    protected abstract void initData(Bundle savedInstanceState);

    protected abstract void initRootView(Bundle savedInstanceState);

    protected abstract void initUI(Bundle savedInstanceState);

    protected abstract void loadData(Bundle savedInstanceState);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        initData(savedInstanceState);
        loading = new DLoading(this);
        super.onCreate(savedInstanceState);
        initRootView(savedInstanceState);
        loadData(savedInstanceState);
        initUI(savedInstanceState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);
    }

    @Override
    public void setTitle(int titleId) {
        super.setTitle(titleId);
    }

    public BaseActivity getContext() {
        return this;
    }


    public void showLoading() {
        if (!loading.isShowing())
            loading.show();
    }

    public void hideLoading() {
        if (loading.isShowing())
            loading.dismiss();
    }

    public void showErrorDialog(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void replaceFragment(int fragmentContent, BaseFragment fragment, boolean addToBackStack) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (addToBackStack) {
            transaction.addToBackStack(fragment.getClass().getName());
        }
        transaction.replace(fragmentContent, fragment);
        transaction.commit();
    }

    public void addFragment(int fragmentContent, BaseFragment fragment, boolean addToBackStack) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (addToBackStack) {
            transaction.addToBackStack(fragment.getClass().getName());
        }
        transaction.add(fragmentContent, fragment);
        transaction.commit();
    }
}
