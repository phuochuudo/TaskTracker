package blueotterdemo.demo.utils;

/**
 * Created by Administrator on 3/13/2018.
 */

public class Constants {
    public static final String KEY_SAVE_ACCESS_TOKEN = "key_save_access_token";
    public static final String TASK_ID = "tasktracker-13032018";
    public static final String USER_ID = "user_id";
    public static final String TASK_SAVED = "Task Saved";
}
