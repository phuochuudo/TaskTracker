package blueotterdemo.demo.utils;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

/**
 * Created by PhuocDH on 3/14/2018.
 */

public class ToastUtil {
    private Toast currentToast;
    private static ToastUtil toastInstance = null;


    public static ToastUtil getInstance() {
        if (toastInstance == null) {
            toastInstance = new ToastUtil();
        }
        return toastInstance;
    }

    public void show(Context context, String message) {
        if (context == null || message == null) {
            return;
        }
        if (toastInstance.currentToast != null) {
            toastInstance.currentToast.cancel();
        }

        if (((Activity) context).isFinishing()) return;

        toastInstance.currentToast = Toast.makeText(context, message
                , Toast.LENGTH_SHORT);
        toastInstance.currentToast.setGravity(Gravity.CENTER, 0, 0);
        toastInstance.currentToast.show();
    }

    /**
     * Option toast will have boolean value to config for long or short time to show
     *
     * @param context
     * @param message
     * @param isShowLong true: Toast.LENGTH_LONG, else Toast.LENGTH_SHORT
     */
    public void show(Context context, String message, boolean isShowLong) {
        if (context == null || message == null) {
            return;
        }
        if (toastInstance.currentToast != null) {
            toastInstance.currentToast.cancel();
        }

        if (((Activity) context).isFinishing()) return;

        toastInstance.currentToast = Toast.makeText(context, message
                , !isShowLong ? Toast.LENGTH_SHORT : Toast.LENGTH_LONG);
        toastInstance.currentToast.setGravity(Gravity.CENTER, 0, 0);
        toastInstance.currentToast.show();
    }
}
